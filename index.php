
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PHP Backend Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <?php include_once 'template/header.php'; ?>
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 ">

            <h1 class="mt-5">Welcome Rockstar!</h1>
            <a href="news/" class="btn btn-primary btn-lg">CLICK HERE TO VIEW OUTPUT</a>
            <p class="lead">Thank you for your interest in taking this exam and giving it a time.
                The objective of this exam is for us to gauge your technical knowledge in using PHP as a web development tool.</p>
            <p>
                Please create a website that has the following use-cases using PHP and MySQL.
                The UI does not need to be fancy/interactive but you are welcome to do it.

                - create news

                - delete news

                - view news

                - create comment

                - delete comment

                - view comments of a news


</p><p>
                Please avoid the following

                - full-stack framework but micro frameworks are welcome.

                - don't use any object relational mapper (ORM). (Just use SQL)

                - don't over engineer the exam.



                We do understand that using such tools can fasten the web development but again the objective of this exam is to gauge your backend skills using PHP.



                Together with this document is a simple application please kindly read the files and tell to us what is wrong with it.

            </p><p>

                What do we expect?

                - A website that has the use-cases mentioned above.

                - The source code and the database design.

                - A document listing down issues found on the existing application.

                Thanks and good luck Rockstar!

            </p>
        </div>
    </div>
</div>


</body>

</html>
