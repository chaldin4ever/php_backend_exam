-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for php_backend_exam
DROP DATABASE IF EXISTS `php_backend_exam`;
CREATE DATABASE IF NOT EXISTS `php_backend_exam` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `php_backend_exam`;

-- Dumping structure for table php_backend_exam.comment
DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `body` mediumtext,
  `created_at` datetime DEFAULT NULL,
  `news_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table php_backend_exam.comment: ~8 rows (approximately)
DELETE FROM `comment`;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` (`id`, `body`, `created_at`, `news_id`) VALUES
	(1, 'i like this news', '2016-11-30 14:21:23', 1),
	(2, 'i have no opinion about that', '2016-11-30 14:24:08', 1),
	(3, 'are you kidding me ?', '2016-11-30 14:28:06', 1),
	(4, 'this is so true', '2016-11-30 17:21:23', 2),
	(5, 'trolololo', '2016-11-30 17:39:25', 2),
	(6, 'luke i am your father', '2016-11-30 14:34:06', 3),
	(11, 'test', '2019-02-20 00:00:00', 1),
	(12, 'test123', '2019-02-20 00:00:00', 6);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;

-- Dumping structure for table php_backend_exam.news
DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(511) DEFAULT NULL,
  `body` mediumtext,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table php_backend_exam.news: ~5 rows (approximately)
DELETE FROM `news`;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `title`, `body`, `created_at`) VALUES
	(1, 'news update 1', 'this is the description of our first news', '2016-11-30 14:18:23'),
	(2, 'news 2', 'this is the description of our second news', '2016-11-30 14:24:23'),
	(3, 'news 3', 'this is the description of our third news', '2016-12-01 04:33:23'),
	(6, 'This is news 123', 'asdf asdf asdf asdf asdfasf', '2019-02-20 06:02:41');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
