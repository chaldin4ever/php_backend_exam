<?php

define('ROOT', __DIR__);
require_once(ROOT . '/../utils/NewsManager.php');
require_once(ROOT . '/../utils/CommentManager.php');

$n = NewsManager::getInstance()->viewNews($_GET['id']);
$news = $n[0];

if($_POST){

    NewsManager::getInstance()->updateNews($_GET['id'], $_POST['title'], $_POST['body']);
    header('Location: /news/?status=success');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PHP Backend Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <?php include_once '../template/header.php'; ?>
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 ">
            <h1 class="mt-5">Edit News </h1>

            <form method="post">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" required id="title" name="title" value="<?php echo $news->getTitle(); ?>" placeholder="Enter News Title">
                </div>


                <div class="form-group">
                    <label for="body">Body</label>
                    <textarea class="form-control" id="body" rows="3" name="body" required placeholder="Enter News Body"><?php echo $news->getBody(); ?></textarea>
                </div>
                <div class="form-group">
                    <a href="/news/" class="btn btn-danger btn-lg float-left mr-2"> <i class="fa fa-times"></i> Cancel</a> <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-save mr-1"></i>  Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
