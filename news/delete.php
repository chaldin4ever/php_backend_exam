<?php

define('ROOT', __DIR__);
require_once(ROOT . '/../utils/NewsManager.php');
require_once(ROOT . '/../utils/CommentManager.php');

if($_POST){
    NewsManager::getInstance()->deleteNews($_POST['id']);

    header('Location: /news/?status=deleted');
}

$n = NewsManager::getInstance()->viewNews($_GET['id']);
$news = $n[0];

$comments = CommentManager::getInstance()->listNewsComments($_GET['id']);


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PHP Backend Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <?php include_once '../template/header.php'; ?>
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 ">
            <h1 class="mt-5">Delete News
                </h1>

            <div class="card" >
                <div class="card-body">
                    <form method="post" action="delete.php?id=<?php echo $_GET['id'];?>">
                        <input type="hidden" name="id" value="<?php echo $news->getId(); ?>" />
                    <div class="alert alert-danger ">
                        <strong>Are you sure you want to delete this news? </strong>

                        <a href="/news/" class="btn btn-danger btn-sm ml-2  "> <i class="fa fa-times"></i> No</a>
                        <button class="btn btn-primary btn-sm "> <i class="fa fa-check"></i> Delete</button>
                    </div>
                    </form>
                    <h5 class="card-title"><?php echo $news->getTitle(); ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted">Created at <?php echo $news->getCreatedAt(); ?></h6>
                    <p class="card-text"><?php echo $news->getBody(); ?></p>
                </div>
                <div class="card-footer text-muted">
                    <strong>Comments</strong>

                    <?php if(!empty($comments)) {
                        foreach ($comments as $k => $comment) {
                            ?>
                            <blockquote class="blockquote">
                                <p class="mb-0"><?php echo $comment->getBody(); ?></p>
                            </blockquote>
                        <?php }
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
