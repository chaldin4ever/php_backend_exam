<?php

define('ROOT', __DIR__);
require_once(ROOT . '/../utils/NewsManager.php');
require_once(ROOT . '/../utils/CommentManager.php');

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PHP Backend Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <?php include_once '../template/header.php'; ?>
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 ">
            <h1 class="mt-5">News <a href="add.php" class="btn btn-primary btn-sm float-right"> <i class="fa fa-plus"></i> Create</a></h1>

            <?php if(isset($_GET['status'])) {
                if ($_GET['status'] == 'success') {
                    ?>
                    <div class="alert alert-success" id="alert-success">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        Successfully saved.
                    </div>
                <?php } ?>

                <?php if($_GET['status'] == 'deleted'){ ?>
                    <div class="alert alert-danger" id="alert-danger">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        Successfully deleted.</div>
                <?php } ?>

            <?php } ?>



            <table class="table table-striped table-bordered ">
                <thead>
                <tr>
                    <th>ID</th>
                    <th >Title</th>
                    <th >Description</th>
                    <th>Created At</th>
                    <th style="width:230px"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach (NewsManager::getInstance()->listNews() as $news) {?>

                    <tr>
                        <td><?php echo $news->getId()?></td>
                        <td><?php echo $news->getTitle()?></td>
                        <td><?php echo substr($news->getBody(), 0, 50).' ..';?></td>
                        <td><?php echo $news->getCreatedAt()?></td>
                        <td>
                            <a href="view.php?id=<?php echo $news->getId()?>" class="btn btn-secondary btn-sm"> <i class="fa fa-eye"></i> View</a>
                            <a href="edit.php?id=<?php echo $news->getId()?>" class="btn btn-secondary btn-sm"> <i class="fa fa-pencil"></i> Edit</a>
                            <a href="delete.php?id=<?php echo $news->getId()?>" class="btn btn-secondary btn-sm"><i class="fa fa-trash"></i> Delete</a>
                        </td>
                    </tr>
              <?php
                }?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
    $("#alert-success, #alert-danger").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert-success").slideUp(500);
    });
</script>
</body>

</html>
